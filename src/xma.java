import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JSeparator;

/**/

/**/

public class xma {

	private JFrame frame;
	


	/**
	 * Create the application.
	 */
	public xma() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setBounds(100, 100, 450, 300);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().getContentPane().setLayout(null);
		
		JLabel lblItIsXmas = new JLabel("Is it Xmas?"); //$NON-NLS-1$
		lblItIsXmas.setFont(new Font("Trajan Pro", Font.PLAIN, 20)); //$NON-NLS-1$
		lblItIsXmas.setBounds(17, 9, 123, 29);
		getFrame().getContentPane().add(lblItIsXmas);
		
		JLabel lblCreatedBySzab = new JLabel("Created by Szabó Szabolcs © 2018."); //$NON-NLS-1$
		lblCreatedBySzab.setFont(new Font("Lucida Grande", Font.PLAIN, 9)); //$NON-NLS-1$
		lblCreatedBySzab.setBounds(275, 256, 169, 16);
		getFrame().getContentPane().add(lblCreatedBySzab);
		
		JLabel xmasLabel = new JLabel(""); //$NON-NLS-1$
		xmasLabel.setHorizontalAlignment(SwingConstants.CENTER);
		xmasLabel.setFont(new Font("Lucida Grande", Font.BOLD, 99)); //$NON-NLS-1$
		xmasLabel.setBounds(17, 70, 412, 162);
		getFrame().getContentPane().add(xmasLabel);
		
		
		JLabel datumLabel = new JLabel("Dátum:");  //$NON-NLS-1$
		datumLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		datumLabel.setBounds(154, 13, 275, 16);
		getFrame().getContentPane().add(datumLabel);
		
		
		
		
		JButton xmasButton = new JButton("Ma karácsony van?"); //$NON-NLS-1$
		xmasButton.addActionListener(e -> {
			
			//getting current date and time using Date class
			
		       DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //$NON-NLS-1$
		       Date dateobj = new Date();
		       System.out.println(df.format(dateobj));

		       /*getting current date time using calendar class 
		        * An Alternative of above*/
		       
		       Calendar calobj = Calendar.getInstance();
		       System.out.println(df.format(calobj.getTime()));
		       
		       datumLabel.setText("Mai nap: " + df.format(calobj.getTime())); //$NON-NLS-1$
		       
		       
		       DateFormat df1 = new SimpleDateFormat("MM-dd"); //$NON-NLS-1$
		       Date dateobj1 = new Date();
		       System.out.println(df1.format(dateobj1));

		       /*getting current date time using calendar class 
		        * An Alternative of above*/
		       
		       Calendar calobj1 = Calendar.getInstance();
		       System.out.println(df1.format(calobj1.getTime()));
		       
		        if( (df1.format(calobj1.getTime())) == "12-25" || (df1.format(calobj1.getTime())) == "12-26") {  //$NON-NLS-1$ //$NON-NLS-2$
		        	
		        		xmasLabel.setText("IGEN"); //$NON-NLS-1$
		        	
		        } else {
		        		xmasLabel.setText("NEM"); //$NON-NLS-1$
		        }
		       	
		});
		
		
		xmasButton.setBounds(135, 38, 169, 29);
		getFrame().getContentPane().add(xmasButton);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(16, 30, 418, 8);
		this.frame.getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(15, 65, 419, 10);
		this.frame.getContentPane().add(separator_1);

	}

	public JFrame getFrame() {
		return this.frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					xma window = new xma();
					window.getFrame().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}
	
}



